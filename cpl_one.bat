:: Dans une fen�tre intel du bon type
@echo off

goto main

::-----------------------------------------------------------------------------
:: The function get_intel_version
::
:: Input:
::
:: Output:
::    RET_1    Stripped value
::
:: https://ss64.com/nt/syntax-replace.html
::-----------------------------------------------------------------------------
:get_intel_version
    setlocal EnableExtensions EnableDelayedExpansion
    ::---  Get first line of stderr for command "icl /QV"
    for /F "tokens=* USEBACKQ" %%F in (`icl /QV 2^>^&1`) do ( 
        set _VER=%%F 
        goto end_for_intel_version
    )
    :end_for_intel_version
    
    ::---  Filter main version number
    set _VER=%_VER:*Version =%
    set _END=%_VER:*.=%
    call set _VER=%%_VER:%_END%=%%
    set _VER=%_VER:.=%
    endlocal & set RET_1=%_VER%
goto :EOF

:get_msvc_version
    setlocal EnableExtensions EnableDelayedExpansion
    set _VER=%VisualStudioVersion%
    
    ::---  Filter main version number
    set _END=%_VER:*.=%
    call set _VER=%%_VER:%_END%=%%
    set _VER=%_VER:.=%
    
    ::---  MSVC version from VS version
    :: https://dev.to/yumetodo/list-of-mscver-and-mscfullver-8nd
    set _MSVC=Undefined
    if %_VER% == 11 set _MSVC=11.0
    if %_VER% == 12 set _MSVC=12.0
    if %_VER% == 14 set _MSVC=14.0
    if %_VER% == 15 set _MSVC=14.1
    if %_VER% == 16 set _MSVC=14.2
    if %_VER% == 17 set _MSVC=14.3
    if %_MSVC% == Undefined (
        echo Unsupported Visual Studio Version: %_VER%
        exit /b 2
    )
   
    endlocal & set RET_1=%_MSVC%
goto :EOF


::-----------------------------------------------------------------------------
:: main()
::-----------------------------------------------------------------------------
:main
    setlocal enableextensions enabledelayedexpansion

    :: ---  Compile b2 if necessary
    if not exist "b2.exe" (
        call bootstrap.bat
    )

    :: ---  Get Intel and MSVC version
    call :get_intel_version & set _INTL=!RET_1!
    call :get_msvc_version  & set _MSVC=!RET_1!
    if %errorLevel% NEQ 0 (exit /b )

    :: ---  Target libs
    set TGTS=
    set TGTS=%TGTS% --with-system
    set TGTS=%TGTS% --with-filesystem
    set TGTS=%TGTS% --with-thread

    :: ---  Options
    set OPTS=
    set OPTS=%OPTS% address-model=64
    set OPTS=%OPTS% variant=release
    set OPTS=%OPTS% toolset=msvc-%_MSVC%
    ::set OPTS=%OPTS% toolset=intel-%_INTL%-vc%_MSVC%
    set OPTS=%OPTS% link=static
    set OPTS=%OPTS% runtime-link=shared

    :: ---  Compile
    del /q /s /f bin.v2 > NUL
    b2 --stagedir=./_run/win64/Release --layout=system %TGTS% %OPTS% variant=release stage
    :: del /q /s /f bin.v2 > NUL
    :: b2 --stagedir=./_run/win64/Debug   --layout=system %TGTS% %OPTS% variant=debug   stage
    ::del /q /s /f bin.v2 > NUL

    endlocal
goto :EOF
